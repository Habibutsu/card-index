from distutils.core import setup
import subprocess

import py2exe,sys,os

def get_revision():
    proc = subprocess.Popen(
        #["git", "describe", "--always"],
        ["git", "rev-list", "HEAD", "--count"],
        stdout=subprocess.PIPE)
    (p_stdout, p_stderr) = proc.communicate()
    return p_stdout.strip().decode("utf-8")

major_version = 0
minor_version = 1
revision = get_revision()

version = "%s.%s.%s" %(major_version, minor_version, revision)

target = {
    'script' : "card_index/__main__.py",
    'version' : version,
    'company_name' : "",
    'copyright' : "GPL",
    'name' : "CardIndex", 
    'dest_base' : "CardIndex", 
    'icon_resources': [(1, "card_index.ico")]
}


setup(
    console=[target],
    data_files=[
        ('data', ['data/data.db']),
        ('.', [
            'C:\Windows\SysWOW64\msvcr100.dll',
            'C:\Windows\SysWOW64\msvcp100.dll']),
    ],
    options={
        "py2exe": {
            "bundle_files": 1,
            "includes": ["sip", "PyQt4.QtGui", "PyQt4.QtCore"],
            "excludes": [
                "unicodedata",
                "hashlib",
                "xml",
                "ssl",
                "socket",
                "lzma",
                "bz2",
                "Tkinter",
                "optparse",
                "pydoc",
                "email",
                "http"
                "urlib"
                "unittest",
                "pdb",
                "argparse",
                #"inspect",    # used in Qt
                #"doctest",
            ],
        }
    }
)