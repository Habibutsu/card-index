# -*- coding: utf-8 -*-
from datetime import date, datetime
import time
import math

from PyQt4.QtCore import (
    Qt,
    QAbstractTableModel
)
from PyQt4.QtGui import (
    QBrush,
    QColor,
    QFont
)

class BaseModel(QAbstractTableModel):
    
    hor_headers = None
    hor_headers_width = None
    sql = None

    def __init__(self, parent, db_cur, where=None, args=None):
        super().__init__(parent)
        self.sql_where = where or {}
        self._data = []
        self._args = args or []
        self.parent = parent
        self.parent.setFont(QFont("Arial", 16, QFont.Normal, False));
        self.db_cur = db_cur
        self.getData()
        self.total_count = len(self._data)

    def getData(self):
        where = " AND ".join([w for w, a in self.sql_where.values()])
        args = []
        for w, a in self.sql_where.values():
            args.extend(a)
        if where:
            where = "WHERE " + where
        sql = self.sql.format(where)
        #print("[SQL] %s /" % sql, self._args + args)
        self.db_cur.execute(sql, self._args + args)
        self._data = self.db_cur.fetchall()

    def headerData(self, col, orientation, role):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return self.hor_headers[col]
            elif role == Qt.SizeHintRole:
                #size = QSize(0, 20)
                # size.setWidth(200)
                #return size
                self.parent.setColumnWidth(col, self.hor_headers_width[col])
        elif orientation == Qt.Vertical and role == Qt.DisplayRole:
            return col + 1
        return None

    def rowCount(self, parent):
        return len(self._data)

    def columnCount(self, parent):
        return len(self.hor_headers)

    def data(self, index, role):
        if not index.isValid():
            return None
        elif role != Qt.DisplayRole:
            return None
        return self._data[index.row()][index.column()]

    def resetWhere(self):
        self.sql_where = {}

class ClientsModel(BaseModel):

    # " 1 + (julianday(valid_to) - julianday(CURRENT_DATE)) -"
    # "   (strftime('%W', valid_to) - strftime('%W', CURRENT_DATE)) * 2,"
    sql = """SELECT fio, count, valid_from, valid_to,
        CASE WHEN active_pause_at IS NULL
            THEN
                CASE WHEN CURRENT_DATE > valid_from
                    THEN julianday(valid_to) - julianday(CURRENT_DATE)
                    ELSE julianday(valid_to) - julianday(valid_from)
                END
            ELSE 'пауза'
        END,
        phone, comment,
        id, valid_to < CURRENT_DATE,
        CAST(strftime('%s', active_pause_at) as int)
    FROM clients {0} ORDER BY fio"""

    hor_headers = [
        "ФИО", "Количество",
        "Действителен с", "Действителен по",
        "Дней осталось", "Телефон", "Комментарий"]
    hor_headers_width = [250, 150, 200, 200, 200, 200, 200]

    def data(self, index, role):
        value = super().data(index, role) 
        if role == Qt.BackgroundRole:
            if self.getIsValid(index.row()):
                return QBrush(QColor(255,200,200))
            elif self.getIsPause(index.row()):
                return QBrush(QColor(200,255,200))
        return value

    def getFIO(self, row):
        return self._data[row][0]

    def getValidFrom(self, row):
        return self._data[row][2]

    def getValidTo(self, row):
        return self._data[row][3]

    def getPhone(self, row):
        return self._data[row][5]

    def getComment(self, row):
        return self._data[row][6]

    def getId(self, row):
        return self._data[row][7]

    def getIsValid(self, row):
        return self._data[row][8]

    def getActivePause(self, row):
        return self._data[row][9]

    def getIsPause(self, row):
        return self.getActivePause(row) is not None

    def hideActive(self):
        self.sql_where["hideActive"] = ("valid_to < CURRENT_DATE", [])

    def hideNotActive(self):
        self.sql_where["hideNotActive"] = ("valid_to > CURRENT_DATE", [])

    def showActive(self):
        self.sql_where.pop("hideActive", None)

    def showNotActive(self):
        self.sql_where.pop("hideNotActive", None)

    def filterByFIO(self, fio):
        if fio:
            self.sql_where["filterByFIO"] = ("UPPER_UTF8(fio) LIKE '%s%%'" % fio.upper(), [])
        else:
            self.sql_where.pop("filterByFIO", None)


class ClientsHistoryModel(BaseModel):

    sql = """SELECT datetime(visit_at, 'localtime'), id
        FROM clients_visits WHERE client_id = ?"""
    hor_headers = ["Дата"]
    hor_headers_width = [300]

    # def data(self, index, role):
    #     value = super().data(index, role) 
    #     if value is not None:
    #         value = datetime.fromtimestamp(value).isoformat()
    #     return value

class ClientPauseHistoryModel(BaseModel):

    sql = """SELECT datetime(pause_at, 'localtime'), operation_type, id
        FROM clients_pauses WHERE client_id = ?"""
    hor_headers = ["Дата", "Тип"]
    hor_headers_width = [300, 200]

    op_type = {0: "Установка", 1: "Снятие"}

    def getData(self):
        super().getData()
        for k, item in enumerate(self._data):
            self._data[k] = (
                item[0],
                self.op_type.get(item[1], "Неизвестно")) + item[1:]


class ClientReportModel(BaseModel):
    
    sql = """SELECT
        id, fio, strftime('%s', valid_from)
        FROM clients
        WHERE valid_to > CURRENT_DATE"""

    sql_visits = """SELECT client_id, strftime('%s', visit_at)
        FROM clients_visits
        WHERE client_id IN ({0}) AND visit_at < CURRENT_TIMESTAMP
        ORDER BY client_id, visit_at"""

    hor_headers = ["ФИО", "Кол-во не посещений", "Последнее посещение"]
    hor_headers_width = [250, 300, 300]

    def get_max_amount_of_skip(self, data):
        skips = []
        for i in range(1, len(data)):
            d1 = datetime.fromtimestamp(data[i-1])
            d2 = datetime.fromtimestamp(data[i])
            # d1 = date.fromtimestamp(data[i-1] + time.timezone)
            # d2 = date.fromtimestamp(data[i] + time.timezone)
            w1 = d1.isocalendar()[1]
            w2 = d2.isocalendar()[1]
            weekends = 0
            skip = (d2 - d1).days - 1
            if w2 < w1:
                # 52 - amount of weeks in year
                weekends = w1 + (52 - w2)
            else:
                weekends = w2 - w1
            skips.append(skip - weekends)

        # valid_data =list(
        #     map(datetime.isoformat,
        #         map(datetime.fromtimestamp,
        #             map(
        #                 lambda x: x + time.timezone,
        #                 data))))
        # print(valid_data, skips)
        return max(skips)

    def __init__(self, parent, db_cur, days):
        self.days = days
        super().__init__(parent, db_cur)

    def getData(self):
        super().getData()

        data = dict(
            (v[0], {"fio": v[1], "history": [int(v[2]) - 24*60*60]}) for v in self._data)

        # select history
        ids = list(data.keys())
        sql = self.sql_visits.format(",".join(['?']*len(ids)))
        self.db_cur.execute(sql, ids)
        history = self.db_cur.fetchall()

        # group by
        for v in history:
            data[v[0]]["history"].append(int(v[1]))

        new_data = []
        for v in data.values():
            max_skip = self.get_max_amount_of_skip(
                v["history"] + [time.time()])
            #print((v["fio"], max_skip))
            if max_skip > self.days:
                if len(v["history"]) == 1:
                    last_visit = "Нет посещений"
                else:
                    # format for output
                    last_visit = datetime.fromtimestamp(
                        int(v["history"][-1])).isoformat()
                new_data.append((
                    v["fio"], max_skip,
                    last_visit))
        self._data = new_data
