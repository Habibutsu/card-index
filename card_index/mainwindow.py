# -*- coding: utf-8 -*-
import os

from datetime import date, datetime
import time

from PyQt4.QtGui import (
    QIcon,
    QLabel,
    QAction,
    QMainWindow,
    QPushButton,
    QSplitter,
    QStyle,
    QTreeWidget,
    QTableWidgetItem,
    QTabWidget,
    QTabBar,
    QTableWidget,
    QTextCursor,
    QKeySequence,
    QMenu,
    QTableView,
    QRadioButton,
    QCheckBox,
    QLineEdit,
    QFont,
    QMessageBox
)

from PyQt4.QtCore import (
    Qt,
    pyqtSlot,
    SIGNAL,
    SLOT,
    QModelIndex
)

from card_index.dialogs.client import NewClient, EditClient
from card_index.dialogs.mark_visit import MarkVisitClient
from card_index.dialogs.get_days import GetDaysDialog

from card_index.models import (
    ClientsModel,
    ClientReportModel,
    ClientsHistoryModel,
    ClientPauseHistoryModel
)


def set_trace():
    from PyQt4.QtCore import pyqtRemoveInputHook
    from pdb import set_trace
    pyqtRemoveInputHook()
    set_trace()

class MainWindow(QMainWindow):

    menu = {}
    config = {}
    views = {}
    type_map = {
            0: "Спортзал",
            1: "Фитнес",
            2: "Детский"
        }

    def __init__(self, parent):
        super().__init__()
        self.parent = parent

        self.setWindowTitle(self.trUtf8("Картотека [Спортзал]"))

    def show(self):
        self.initUI()
        super().show()

    def initUI(self):
        self.setGeometry(100, 200, 1100, 400)
        self.createMenus()

        self.tab_widget = QTabWidget()
        tab_bar = self.tab_widget.tabBar()
        self.tab_widget.setTabsClosable(True)
        self.tab_widget.tabCloseRequested.connect(self.closeTab)

        self.toolbar = self.addToolBar('Filters')


        self.isHideActive = QCheckBox(self.trUtf8("Скрыть активные"))
        self.isHideActive.setLayoutDirection(Qt.RightToLeft);
        self.isHideActive.stateChanged.connect(self.hideActiveClients)

        self.isHideNotActive = QCheckBox(self.trUtf8("Скрыть не активные"))
        self.isHideNotActive.setLayoutDirection(Qt.RightToLeft);
        self.isHideNotActive.stateChanged.connect(self.hideNotActiveClients)

        self.toolbar.addWidget(self.isHideActive)
        self.toolbar.addWidget(self.isHideNotActive)

        self.fio_input = QLineEdit()
        self.fio_input.setFixedWidth(200)
        self.toolbar.addWidget(QLabel("Поиск по ФИО:"))
        self.toolbar.addWidget(self.fio_input)

        # exitAction = QAction(QIcon('/home/a-verbitsky/DEVELOP/card-index/card_index/resources/exit.png'), 'Exit', self)
        # # exitAction.setShortcut('Ctrl+Q')
        # exitAction.triggered.connect(self.close)
        # self.toolbar.addAction(exitAction)
        # self.tab_widget.tabCloseRequested.connect(self.closeTab)
        searchAction = QAction(
            QIcon(os.path.join(self.parent.base_path, "resources", "search.png")),
            'Search', self)
        searchAction.triggered.connect(self.searchClient)
        self.toolbar.addAction(searchAction)


        cancelAction = QAction(
            QIcon(os.path.join(self.parent.base_path, "resources", "cancel.png")),
            'Cancel', self)
        cancelAction.triggered.connect(self.resetFilter)
        self.toolbar.addAction(cancelAction)

        self.status_bar = self.statusBar()
        self.createClientsView()

        self.setCentralWidget(self.tab_widget)

    def hideActiveClients(self, state):
        clients_model = self.clients_view.model()
        if state == Qt.Checked:
            clients_model.hideActive()
        else:
            clients_model.showActive()
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def hideNotActiveClients(self, state):
        clients_model = self.clients_view.model()
        if state == Qt.Checked:
            clients_model.hideNotActive()
        else:
            clients_model.showNotActive()
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def markClientAction(self):
        dialog = MarkVisitClient(self)
        dialog.show()

    def createClientsView(self):
        table_view = QTableView()
        model = ClientsModel(table_view, self.db_cur)
        table_view.setModel(model)

        ctx_menu = QMenu()
        mark_action = ctx_menu.addAction(self.trUtf8("Отметить"))
        mark_action.triggered.connect(self.markClient)

        edit_action = ctx_menu.addAction(self.trUtf8("Изменить"))
        edit_action.triggered.connect(self.editClient)

        view_report = ctx_menu.addAction(self.trUtf8("История посещений"))
        view_report.triggered.connect(self.createClientsHistoryView)

        pause_action = ctx_menu.addAction(self.trUtf8("Поставить/снять паузу"))
        pause_action.triggered.connect(self.pauseClient)

        pause_history = ctx_menu.addAction(self.trUtf8("История пауз"))
        pause_history.triggered.connect(self.createClientPauseHistoryView)

        delete_action = ctx_menu.addAction(self.trUtf8("Удалить"))
        delete_action.triggered.connect(self.deleteClient)

        def ctx_menu_request(pos):
            ctx_menu.exec_(self.mapToGlobal(pos)) 

        table_view.setContextMenuPolicy(Qt.CustomContextMenu)
        table_view.customContextMenuRequested.connect(ctx_menu_request)
        
        self.tab_widget.insertTab(0, table_view, self.trUtf8("Клиенты"))
        self.clients_view = table_view

        self.status_bar.addPermanentWidget(
            QLabel(self.trUtf8("Общее количество клиентов: %s") % model.total_count))

    def createClientsHistoryView(self):
        cur_row = self.clients_view.currentIndex().row()
        clients_model = self.clients_view.model()

        title = "Посещения/%s" % clients_model.getFIO(cur_row)
        #title = model.data(model.index(cur_index.row(), 1), Qt.DisplayRole)
        client_id = clients_model.getId(cur_row)

        table_view = QTableView()
        model = ClientsHistoryModel(table_view, self.db_cur, args=[client_id])
        table_view.setModel(model)
        count_tabs = self.tab_widget.count()
        self.tab_widget.insertTab(count_tabs, table_view, title)
        self.tab_widget.setCurrentIndex(count_tabs)

    def createClientPauseHistoryView(self):
        cur_row = self.clients_view.currentIndex().row()
        clients_model = self.clients_view.model()

        title = "Паузы/%s" % clients_model.getFIO(cur_row)
        #title = model.data(model.index(cur_index.row(), 1), Qt.DisplayRole)
        client_id = clients_model.getId(cur_row)

        table_view = QTableView()
        model = ClientPauseHistoryModel(table_view, self.db_cur, args=[client_id])
        table_view.setModel(model)
        count_tabs = self.tab_widget.count()
        self.tab_widget.insertTab(count_tabs, table_view, title)
        self.tab_widget.setCurrentIndex(count_tabs)

    def createMenus(self):
        menubar = self.menuBar()
        clients_menu = menubar.addMenu(self.trUtf8("&Клиенты"))
        db_menu = menubar.addMenu(self.trUtf8("&База"))

        newClientAction = clients_menu.addAction(self.trUtf8("Добавить"))
        newClientAction.triggered.connect(self.addClient)

        reportClientAction = clients_menu.addAction(self.trUtf8("Отчет по не посещениям"))
        reportClientAction.triggered.connect(self.reportClientAction)

        exitAction = clients_menu.addAction(self.trUtf8("Выход"))
        exitAction.triggered.connect(self.close)

        self.menu_action_gym = db_menu.addAction(self.trUtf8("Спортазaл"))
        self.menu_action_gym.setEnabled(False)
        self.menu_action_gym.triggered.connect(self.selectGymDb)
        self.menu_actison_fitness = db_menu.addAction(self.trUtf8("Фитнесс"))
        self.menu_actison_fitness.triggered.connect(self.selectFitnessDb)

    def selectGymDb(self):
        self.menu_action_gym.setEnabled(False)
        self.menu_actison_fitness.setEnabled(True)
        self.db_cur.close()
        self.db_conn = self.db_conns["gym"]
        self.db_cur = self.db_conn.cursor()

        self.setWindowTitle(self.trUtf8("Картотека [Спортзал]"))
        for num in range(1, self.tab_widget.count()):
            self.tab_widget.removeTab(num)

        clients_model = self.clients_view.model()
        clients_model.db_cur = self.db_cur
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def selectFitnessDb(self):
        self.menu_action_gym.setEnabled(True)
        self.menu_actison_fitness.setEnabled(False)
        self.db_cur.close()
        self.db_conn = self.db_conns["fitness"]
        self.db_cur = self.db_conn.cursor()

        self.setWindowTitle(self.trUtf8("Картотека [Фитнесс]"))
        for num in range(1, self.tab_widget.count()):
            self.tab_widget.removeTab(num)

        clients_model = self.clients_view.model()
        clients_model.db_cur = self.db_cur
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def resetFilter(self):
        clients_model = self.clients_view.model()
        self.fio_input.setText("")
        clients_model.resetWhere()
        self.isHideActive.setChecked(False)
        self.isHideNotActive.setChecked(False)
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def searchClient(self):
        clients_model = self.clients_view.model()
        clients_model.filterByFIO(self.fio_input.text())
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def addClient(self):
        dialog = NewClient(self)
        dialog.exec_()

        if not dialog.data:
            return

        self.db_cur.execute(
            """INSERT INTO clients
                (fio, valid_from, valid_to, phone, comment)
            VALUES (?, ?, ?, ?, ?)""",
            dialog.data)
        self.db_conn.commit()
        clients_model = self.clients_view.model()
        clients_model.getData()
        clients_model.layoutChanged.emit()
        #clients_model.emit(SIGNAL("layoutChanged()"))

    def editClient(self):
        cur_row = self.clients_view.currentIndex().row()
        clients_model = self.clients_view.model()

        dialog = EditClient(self,
            clients_model.getFIO(cur_row),
            clients_model.getValidFrom(cur_row),
            clients_model.getValidTo(cur_row),
            clients_model.getPhone(cur_row),
            clients_model.getComment(cur_row))
        dialog.exec_()

        if not dialog.data:
            return

        self.db_cur.execute(
            """UPDATE clients
            SET fio =?, valid_from =?, valid_to=?, phone=?, comment=?
            WHERE id = ?""",
            dialog.data + (clients_model.getId(cur_row),))
        self.db_conn.commit()
        clients_model = self.clients_view.model()
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def markClient(self):
        # dialog = MarkVisitClient(self)
        # dialog.exec_()

        # if dialog.data is None:
        #     return
        # visit_dt = dialog.dt

        visit_dt = datetime.now()

        cur_row = self.clients_view.currentIndex().row()
        clients_model = self.clients_view.model()
        client_id = clients_model.getId(cur_row)

        if clients_model.getIsPause(cur_row):
            QMessageBox.warning(
                self, self.trUtf8("На паузе"),
                self.trUtf8("Клиент поставлен на паузу, отмечать запрешено"))
            return

        self.db_cur.execute("""SELECT count(*), visit_at FROM clients_visits
            WHERE client_id = ? AND date(visit_at) = CURRENT_DATE""", (client_id,))
        (count, visit_at) = self.db_cur.fetchone()

        if count != 0:
            QMessageBox.warning(
                self, self.trUtf8("Уже отмечен"),
                self.trUtf8("Клиент уже отмечен: %s") % visit_at)
            return

        self.db_cur.execute(
            "UPDATE clients SET count = count + 1 WHERE id = ?",
            [client_id])

        # for case when data is taken from dialog
        self.db_cur.execute("""INSERT INTO clients_visits 
                (client_id, visit_at)
                VALUES (?, datetime(?, 'utc'))""",
                (client_id, visit_dt.isoformat()))

        # self.db_cur.execute("""INSERT INTO clients_visits 
        #         (client_id, visit_at)
        #         VALUES (?, CURRENT_TIMESTAMP)""",
        #         [client_id])

        self.db_conn.commit()
        clients_model.getData()

    def deleteClient(self):
        reply = QMessageBox.question(
            self, self.trUtf8("Удаление"),
            self.trUtf8("Вы действительно хотите удалить клиента"),
            QMessageBox.Yes|QMessageBox.No)
        if reply == QMessageBox.No:
            return

        cur_row = self.clients_view.currentIndex().row()
        clients_model = self.clients_view.model()
        client_id = clients_model.getId(cur_row)

        self.db_cur.execute(
            "DELETE FROM clients WHERE id = ?",
            [client_id])
        self.db_cur.execute(
            "DELETE FROM clients_visits WHERE client_id = ?",
            [client_id])
        self.db_conn.commit()
        clients_model.getData()
        clients_model.layoutChanged.emit()

    def reportClientAction(self):
        dialog = GetDaysDialog(self)
        dialog.exec_()
        if not dialog.days:
            return
        
        table_view = QTableView()
        model = ClientReportModel(table_view, self.db_cur, int(dialog.days))

        table_view.setModel(model)

        count_tabs = self.tab_widget.count()
        self.tab_widget.insertTab(
            count_tabs, table_view,
            self.trUtf8("Не посещения более %s дней") % dialog.days)
        self.tab_widget.setCurrentIndex(count_tabs)

    def pauseClient(self):
        cur_row = self.clients_view.currentIndex().row()
        clients_model = self.clients_view.model()
        client_id = clients_model.getId(cur_row)

        if clients_model.getIsValid(cur_row):
            QMessageBox.warning(
                self, self.trUtf8("Клиент не активен"),
                self.trUtf8("Нельзя поставить на паузу - продлите срок действия"))
            return
        if clients_model.getIsPause(cur_row):
            d1 = datetime.fromtimestamp(clients_model.getActivePause(cur_row))
            if not (datetime.now() - d1).days:
                QMessageBox.warning(
                    self, self.trUtf8("Снятие с паузы"),
                    self.trUtf8("Снять с паузы можно не раньше чем через сутки"))
                return
            self.db_cur.execute(
                """UPDATE clients SET
                    active_pause_at = NULL,
                    valid_to = date(
                        valid_to,
                        '+' ||
                        CAST(julianday(CURRENT_TIMESTAMP) - julianday(active_pause_at) as integer) ||
                        ' days')
                WHERE id = ?""",
                [client_id])
            op_type = 1
        else:
            self.db_cur.execute(
                "UPDATE clients SET active_pause_at = CURRENT_TIMESTAMP WHERE id = ?",
                [client_id])
            op_type = 0

        self.db_cur.execute("""INSERT INTO clients_pauses 
                (client_id, pause_at, operation_type)
                VALUES (?, CURRENT_TIMESTAMP, ?)""", [client_id, op_type])
        self.db_conn.commit()
        clients_model.getData()

    @pyqtSlot(int)
    def closeTab(self, num):
        if num == 0:
            self.close()
            return
        self.tab_widget.removeTab(num)
