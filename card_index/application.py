# -*- coding: utf-8 -*-

from PyQt4.QtGui import (
    QApplication,
    QFont
)

from card_index.mainwindow import MainWindow

import sys
import os
import sqlite3
import ctypes

DATAFILE_GYM = "data_gym.db"
DATAFILE_FITNESS = "data_fitness.db"

class MainApplication(QApplication):
    mainwindow = None
    db_conn = None
    db_cur = None

    def exec_(self):

        self.setFont(QFont("Arial", 13, QFont.Normal, False));
        #self.settings = QSettings()
        self.mainwindow = MainWindow(self)
        self.mainwindow.db_cur = self.db_cur
        self.mainwindow.db_conn = self.db_conn
        self.mainwindow.db_conns = self.db_conns
        self.mainwindow.show()

        return super(MainApplication, self).exec_()


def get_base_path():
    base_path = None
    encoding = sys.getfilesystemencoding()

    if hasattr(sys, "frozen") and sys.frozen:
        base_path = sys.executable
    else:
        base_path = __file__

    return os.path.dirname(base_path)

def upper_utf8(text):
    return text.upper()

MIGRATIONS = [
    (
        1,
        "Create table for clients",
        """CREATE TABLE clients (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            fio             TEXT NOT NULL,
            count           INTEGER DEFAULT 0,
            valid_from      TIMESTAMP without time zone DEFAULT CURRENT_DATE,
            valid_to        TIMESTAMP without time zone DEFAULT CURRENT_DATE,
            phone           TEXT
        );"""
    ),
    (
        2,
        "Create table for history visits of clients",
        """CREATE TABLE clients_visits (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            client_id       INTEGER,
            visit_at        TIMESTAMP without time zone DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY(client_id) REFERENCES clients(id)
        );"""
    ),
    (
        3,
        "Adding field 'comment' in table 'clients'",
        """ALTER TABLE clients ADD COLUMN comment TEXT;"""
    ),
    (
        4,
        "Adding field 'active_pause_at' in table 'clients'",
        """ALTER TABLE clients ADD COLUMN active_pause_at TIMESTAMP;"""
    ),
    (
        5,
        "Create table 'clients_pauses'",
        """CREATE TABLE clients_pauses (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            client_id       INTEGER,
            pause_at        TIMESTAMP without time zone DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY(client_id) REFERENCES clients(id)
        );"""
    ),
    (
        6,
        "Adding field 'operation_type' in table 'clients_pauses'",
        "ALTER TABLE clients_pauses ADD COLUMN operation_type INTEGER;"
        # 0 - start pause
        # 1 - stop pause
    ),
    (
        7,
        "Correcting datetime in history of visits",
        "UPDATE clients_visits SET visit_at = datetime(visit_at, 'utc')"
    )
]

def db_migrate(conn):
    cur = conn.cursor()
    try:
        cur.execute("SELECT num FROM _migrations ORDER BY id DESC LIMIT 1")
        result = cur.fetchone()
        num = 0 if result is None else result[0]
    except:
        print("Init database")
        num = 0
        cur.execute("""CREATE TABLE _migrations (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            num             INTEGER DEFAULT 0,
            name            TEXT
        );""")

    for num, name, sql in MIGRATIONS[num:]:
        print("%s - %s" % (num, name))
        cur.execute(sql)
        cur.execute(
            "INSERT INTO _migrations (num, name) VALUES (?, ?)",
            [num, name])
        conn.commit()
    else:
        print("Nothing to migrate - up to date")
    cur.close()


def main():
    print("Initialize")
    base_path = get_base_path()

    # TODO: use QSqlDatabase and QSqlQueryModel
    print("Connect to databases")
    db_conns = {
        "gym": sqlite3.connect(
                    os.path.join(base_path, "data", DATAFILE_GYM)),
        "fitness": sqlite3.connect(
                    os.path.join(base_path, "data", DATAFILE_FITNESS))
    }
    print("Preconfigure database")
    db_conns["gym"].create_function("UPPER_UTF8", 1, upper_utf8)
    db_conns["fitness"].create_function("UPPER_UTF8", 1, upper_utf8)

    for dbname, conn in db_conns.items():
        print("Migrate %s" % dbname)
        db_migrate(conn)

    app = MainApplication(sys.argv)
    app.base_path = base_path
    app.db_conns = db_conns
    app.db_conn = db_conns["gym"]
    app.db_cur = app.db_conn.cursor()
    sys.exit(app.exec_())
