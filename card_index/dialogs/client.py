# -*- coding: utf-8 -*-

from PyQt4.QtCore import (
    Qt,
    QSize,
    SIGNAL,
    QRegExp,
    QCoreApplication,
)

from PyQt4.QtGui import (
    QDialog,
    QVBoxLayout,
    QHBoxLayout,
    QPushButton,
    QComboBox,
    QLabel,
    QLineEdit,
    QGridLayout,
    QDialogButtonBox,
    QIntValidator,
    QRegExpValidator,
    QValidator,
    QCalendarWidget,
    QPlainTextEdit,
)

import time
from datetime import date, datetime, timedelta
import calendar
from functools import partial

def add_month(d):
    if d.month == 12:
        try:
            return d.replace(year=d.year+1, month=1)
        except ValueError:
            return d.replace(year=d.year+1, month=1, day=calendar.mdays[1])
    else:
        try:
            return d.replace(month=d.month+1)
        except ValueError:
            return d.replace(month=d.month+1, day=calendar.mdays[d.month + 1])


class NewClient(QDialog):
    types = [
        "Спортзал",
        "Фитнес",
        "Детский"]

    def __init__(self, parent):
        super().__init__(parent)
        self.data = None
        self.setWindowTitle(self.trUtf8("Новый клиент"))
        self.setMaximumSize(QSize(0, 0))
        self.initLayout()

    def initLayout(self):
        layout = QGridLayout(self)

        row = 0

        self.fio_input = QLineEdit()
        layout.addWidget(QLabel("ФИО"), row, 0, Qt.AlignLeft)
        layout.addWidget(self.fio_input, row, 1)
        row+=1

        self.phone_input = QLineEdit()
        layout.addWidget(QLabel("Телефон"), row, 0, Qt.AlignLeft)
        layout.addWidget(self.phone_input, row, 1)
        row+=1
        # layout.addWidget(QLabel("В формате: +"), row, 1, Qt.AlignLeft)
        # row+=1

        self.valid_from = QCalendarWidget()
        self.valid_from.selectionChanged.connect(self.buttonReset)
        layout.addWidget(
            QLabel("Действителен с"), row, 0, Qt.AlignLeft|Qt.AlignTop)
        layout.addWidget(self.valid_from, row, 1)
        row+=1

        button_layout = QHBoxLayout()

        self.buttons = []

        self.buttons.append((1, QPushButton("+1 мес.", self)))
        self.buttons.append((3, QPushButton("+3 мес.", self)))
        self.buttons.append((6, QPushButton("+6 мес.", self)))
        self.buttons.append((12, QPushButton("+12 мес.", self)))

        self.buttons[0][1].setFlat(True)

        for idx, (_, btn) in enumerate(self.buttons):
            button_layout.addWidget(btn)
            btn.clicked.connect(partial(self.buttonClick, idx))

        layout.addLayout(button_layout, row, 1)
        row+=1

        self.valid_to = QCalendarWidget()
        self.valid_to.setSelectedDate(add_month(datetime.now()))
        self.valid_to.selectionChanged.connect(self.buttonReset)

        layout.addWidget(
            QLabel("Действителен по"), row, 0, Qt.AlignLeft|Qt.AlignTop)
        layout.addWidget(self.valid_to, row, 1)
        row+=1

        self.comment = QPlainTextEdit()
        layout.addWidget(
            QLabel("Комментарий"), row, 0, Qt.AlignLeft|Qt.AlignTop)
        layout.addWidget(self.comment, row, 1)
        row+=1

        # self.visit_type = QComboBox()
        # self.visit_type.setEnabled(False)
        # for t in self.types:
        #     self.visit_type.addItem(t)

        # layout.addWidget(QLabel("Тип"), row, 0, Qt.AlignLeft)
        # layout.addWidget(self.visit_type, row, 1)
        # row+=1

        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)
        #buttons.button(QDialogButtonBox.Ok).setText("Добавить")

        buttons.accepted.connect(self.onAccept)
        buttons.rejected.connect(self.close)

        layout.addWidget(buttons, row, 1)
        layout.setColumnStretch(0, 0)
        self.resize(300, layout.heightForWidth(300))
        self.setModal(True)
        self.setLayout(layout)

    def buttonClick(self, idx):
        month = self.buttons[idx][0]
        d = self.valid_from.selectedDate().toPyDate()
        for _ in range(0, month):
            d = add_month(d)
        self.valid_to.setSelectedDate(d)
        #self.buttonReset()
        self.buttons[idx][1].setFlat(True)

    def buttonReset(self):
        for (_, btn) in self.buttons:
            btn.setFlat(False)

    def onAccept(self):
        self.data = (
            self.fio_input.text(),
            self.valid_from.selectedDate().toPyDate(),
            self.valid_to.selectedDate().toPyDate(),
            self.phone_input.text(),
            self.comment.toPlainText())
        self.close()

class EditClient(NewClient):

    def __init__(self, parent, fio, valid_from, valid_to, phone, comment):
        super().__init__(parent)
        self.fio_input.setText(fio)
        self.phone_input.setText(phone)

        self.valid_from.setSelectedDate(
            datetime.strptime(valid_from, "%Y-%m-%d"))
        self.valid_to.setSelectedDate(
            datetime.strptime(valid_to, "%Y-%m-%d"))

        self.comment.setPlainText(comment)
        self.setWindowTitle(self.trUtf8("Изменение клиента"))