# -*- coding: utf-8 -*-

from PyQt4.QtCore import (
    Qt,
    QSize,
    SIGNAL,
    QRegExp,
    QCoreApplication,
    QTime
)

from PyQt4.QtGui import (
    QDialog,
    QVBoxLayout,
    QHBoxLayout,
    QPushButton,
    QLabel,
    QLineEdit,
    QGridLayout,
    QDialogButtonBox,
    QIntValidator,
    QRegExpValidator,
    QValidator,
    QCalendarWidget,
    QTimeEdit,
    QComboBox,
)

import time
from datetime import datetime

class MarkVisitClient(QDialog):

    def __init__(self, parent):
        super().__init__(parent)
        self.data = None
        self.setWindowTitle("Отметить")
        self.setMaximumSize(QSize(0, 0))
        self.initLayout()

    def initLayout(self):
        layout = QGridLayout(self)

        self.visit_time = QTimeEdit()
        self.visit_time.setDisabled(True)
        cur_time = datetime.now()
        self.visit_time.setTime(QTime(cur_time.hour, cur_time.minute))

        self.visit_date = QCalendarWidget()
        #self.visit_date.setDisabled(True)
        layout.addWidget(
            QLabel("Время"), 0, 0, Qt.AlignLeft|Qt.AlignTop)
        layout.addWidget(self.visit_date, 0, 1)
        layout.addWidget(self.visit_time, 1, 1)

        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        self.connect(buttons, SIGNAL("accepted()"), self.onAccepted)
        self.connect(buttons, SIGNAL("rejected()"), self.close)

        layout.addWidget(buttons, 2, 1)
        layout.setColumnStretch(0, 0)
        self.resize(600, layout.heightForWidth(600))
        self.setModal(True)
        self.setLayout(layout)

    def onAccepted(self):
        self.dt = datetime.combine(
            self.visit_date.selectedDate().toPyDate(),
            self.visit_time.time().toPyTime())

        self.close()
