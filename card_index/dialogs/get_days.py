# -*- coding: utf-8 -*-

from PyQt4.QtCore import (
    Qt,
    QSize,
    SIGNAL,
    QRegExp,
    QCoreApplication,
    QTime
)

from PyQt4.QtGui import (
    QDialog,
    QVBoxLayout,
    QHBoxLayout,
    QPushButton,
    QLabel,
    QLineEdit,
    QGridLayout,
    QDialogButtonBox,
    QIntValidator,
    QRegExpValidator,
    QValidator,
    QCalendarWidget,
    QTimeEdit,
    QComboBox,
)

import time
from datetime import datetime

class GetDaysDialog(QDialog):

    days = None

    def __init__(self, parent):
        super().__init__(parent)
        self.setWindowTitle(self.trUtf8("Количество дней"))
        self.setMaximumSize(QSize(0, 0))
        self.initLayout()

    def initLayout(self):
        layout = QGridLayout(self)

        self.days_input = QLineEdit()
        self.days_input.setValidator(QIntValidator())
        self.days_input.setText("3")
        layout.addWidget(QLabel("Количество дней"), 0, 0, Qt.AlignLeft)
        layout.addWidget(self.days_input, 0, 1)

        buttons = QDialogButtonBox(
            QDialogButtonBox.Ok | QDialogButtonBox.Cancel)

        buttons.accepted.connect(self.onAccept)
        buttons.rejected.connect(self.close)

        layout.addWidget(buttons, 1, 1)
        layout.setColumnStretch(0, 0)
        self.resize(600, layout.heightForWidth(600))
        self.setModal(True)
        self.setLayout(layout)

    def onAccept(self):
        self.days = self.days_input.text()
        return self.close()