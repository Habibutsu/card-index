#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import sqlite3

for db_type in ["gym", "fitness"]:
    conn = sqlite3.connect('card_index/data/data_%s.db' % db_type)
    c = conn.cursor()

    c.execute("""
        CREATE TABLE clients (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            fio             TEXT NOT NULL,
            count           INTEGER DEFAULT 0,
            valid_from      TIMESTAMP without time zone DEFAULT CURRENT_DATE,
            valid_to        TIMESTAMP without time zone DEFAULT CURRENT_DATE,
            phone           TEXT
        );
    """)

    c.execute("""
        CREATE TABLE clients_visits (
            id              INTEGER PRIMARY KEY AUTOINCREMENT,
            client_id       INTEGER,
            visit_at        TIMESTAMP without time zone DEFAULT CURRENT_TIMESTAMP,
            FOREIGN KEY(client_id) REFERENCES clients(id)
        );
    """)

    c.execute("""INSERT INTO clients (fio) VALUES ('Иванов Иван Иванович')""")

    conn.commit()
    conn.close()