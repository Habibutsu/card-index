# -*- coding: utf-8 -*-
from cx_Freeze import setup, Executable

build_exe_options = {
    # "compressed": True,
    "packages": ["os"],
    "include_msvcr": True,
    "includes": [
        "sip",
        "PyQt4.QtGui",
        "PyQt4.QtCore"],
    "excludes": [
        "tkinter"
        # #"unicodedata",
        "hashlib",
        "xml",
        "ssl",
        "socket",
        "lzma",
        "bz2",
        "Tkinter",
        "optparse",
        "pydoc",
        "email",
        "http"
        "urlib"
        "unittest",
        "pdb",
        "argparse",
        #"inspect",    # used in Qt
        #"doctest",
    ],
    "include_files": [
        ("card_index/resources/search.png", "resources/search.png"),
        ("card_index/resources/cancel.png", "resources/cancel.png"),
    ]
}

setup(
    name = "CardIndex",
    author = 'Habibutsu',
    version = "0.1.16",
    description = "Simple realization of card-index of clients for sports gym",
    options = {"build_exe": build_exe_options},
    executables = [
        Executable(
            script="card_index/__main__.py",
            targetName="CardIndex.exe",
            icon="card_index.ico",
            #base="Win32GUI"
            base="Console"
        )
    ])